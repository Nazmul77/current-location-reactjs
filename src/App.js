import React, { Component, Text } from 'react'

import { GOOGLE_API_KEY } from './config'
// import TaskManager from 'react-drag-taskmanager'

import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';


const mapStyles = {
  width: '100%',
  height: '100%'
};


export class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: 'Muhammad Nazmul Hossain',
      latitude: null,
      longitude: null,
      userAddress: null,
      newLatitude: null,
      newLongitude: null


    }
    this.getLocation = this.getLocation.bind(this);

    this.reverseGeocodeCoordinates = this.reverseGeocodeCoordinates.bind(this);
    this.getCoordinates = this.getCoordinates.bind(this);

  }

  //   componentDidMount() {
  //     this.getLocation(); // Get users current location
  // }

  // componentDidUpdate(prevProps, prevState) {
  //     // If prevState changes
  //     if (prevState.newLatitude !== this.state.newLatitude) {
  //         // calculate the distance between initial and new coordinates
  //         this.getDistance();
  //     }
  // }

  // componentWillUnmount() {
  //     // Remember to clear all listeners
  //     navigator.geolocation.clearWatch(this.watcher);
  // }
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getCoordinates, this.handleLocationError);

    }
    else {
      alert("Gelocation is not support.");
    }
    this.watchUserPosition();
    // this.writeUserData();


  };







  // writeUserData = () => {
  //   Firebase.database().ref('maps/').set(this.state);
  //   console.log('DATA SAVED');
  // }

  // getUserData = () => {
  //   let ref = Firebase.database().ref('maps/');
  //   ref.on('value', snapshot => {
  //     const state = snapshot.val();
  //     this.setState(state);
  //   });
  //   console.log('DATA RETRIEVED');
  // }

  getCoordinates(position) {

    console.log(position);
    this.setState({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,

    })
    this.reverseGeocodeCoordinates();
  }






  reverseGeocodeCoordinates() {
    fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${this.state.latitude},${this.state.longitude}&sensor=false&key=${GOOGLE_API_KEY}`)
      .then(response => response.json())
      .then(data => this.setState({
        userAddress: data.results[0].formatted_address,

      }))

      .catch(error => alert(error))
  }



  watchUserPosition() {
    this.watcher = navigator.geolocation.watchPosition(
      (position) => {
        this.setState({
          newLatitude: position.coords.latitude,
          newLongitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 10 },
    );
  }


  // getDistance() {
  //   let initial = {latitude: this.state.latitude, longitude: this.state.longitude};
  //   let newCoord = {latitude: this.state.newLatitude, longitude: this.state.newLongitude};
  //   const distance = geolib.getDistance(initial, newCoord);
  //   if (distance >= 100) {
  //       alert("Success!", "You have reached 100meters!");
  //       this.sendToFirebase(); // whatever you are saving to firebase
  //   }
  // }

  handleLocationError(error) {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        alert("User denied the request for Geolocation.")
        break;
      case error.POSITION_UNAVAILABLE:
        alert("Location information is unavailable.")
        break;
      case error.TIMEOUT:
        alert("The request to get user location timed out.")
        break;
      case error.UNKNOWN_ERROR:
        alert("An unknown error occurred.")
        break;
      default:
        alert("An unknown error occurred.")
    }
  }

  render() {
    return (
      <div style={{ margin: '10px' }}>
        <h2>
          Current Location
        </h2>
        <button onClick={this.getLocation}  >Get Coordinates</button>
        <p>Latitude: {this.state.latitude}</p>
        <p>Longitude: {this.state.longitude} </p>
        <p> Address: {this.state.userAddress}</p>
        <p> Name: {this.state.name}</p>
        <h4> Google Maps </h4>


        {

          this.state.latitude && this.state.longitude ?
            <img src={`https://maps.googleapis.com/maps/api/staticmap?center=${this.state.latitude},${this.state.longitude}&zoom=14&size=1000x1000&sensor=false&markers=color:red%7c${this.state.latitude},${this.state.longitude}&key=${GOOGLE_API_KEY}`} alt=''></img>
            :
            null
        }

        {/* <Map
          google={this.props.google}
          zoom={14}
          style={mapStyles}
          initialCenter={{
            lat: 23.9535742,
            lng: 90.14949879999999 ,
          }}
        >
         <Marker
          onClick={this.onMarkerClick}
          name={'This is test name'}
        />
        </Map> */}

      </div>
    )
  }
}


export default GoogleApiWrapper({
  apiKey: GOOGLE_API_KEY
})(App);
